package dht

import (
	"fmt"
	"log"
	//"math/big"
	//"math"
	"math/rand"
	//"strings"
	"time"
)

const (
	bits             = 160
	successors       = 10
	tickerMultiplier = 2000 //60000 // tickerMultiplier * milliseconds
)

const (
	// method flags
	MTH_ADD = 1 << iota // Add a node to the ring
	MTH_LOOKUP
	MTH_PRINT
	MTH_SETPRED   // Set predecessor
	MTH_STABILIZE // Force stabilize
	MTH_UPDATE
	MTH_SUCCRING // For building the successor list.
)

const (
	// traffic flags
	TRF_ACK  = 1 << iota // indicates the message is a response
	TRF_RSVP             // indicates that a response should be provided
	TRF_PROP             // indicates that the message should be passed forward.
)

//var successors = int(math.Log(math.Pow(2, bits)))

type DHTNode struct {
	successor   Contact
	predecessor Contact
	identity    Contact
	fingers     [bits]Contact
	succList    [successors]Contact
	valid       bool
	transport   *Transport
	queue       chan Msg
	ticker      *time.Ticker
}

func makeDHTNode(id *string, ip, port string) *DHTNode {
	node := DHTNode{}
	node.ticker = time.NewTicker(time.Millisecond * tickerMultiplier) //Need to start goTick as a routine when node is added to ring.
	node.identity = Contact{Id: *id, Ip: ip, Port: port}
	node.queue = make(chan Msg)
	if id == nil {
		node.identity.Id = generateNodeId()
	}
	node.transport = MakeTransport(ip+":"+port, node.identity.Id)
	go node.transport.listen(node.queue)
	go messageHandler(node.queue, &node)
	log.Printf("Created node %x (\"%[1]s\") on %s.", node.identity.Id, node.identity.Address())
	return &node
}

// Notifies the node that the sender is the predecessor
func (node *DHTNode) notify() {
	log.Printf("Node %x sends NOTIFY to %x.", node.identity.Id, node.successor.Id)
	data, ok := node.identity.ToJSON()
	if ok && (node.successor.Id != node.identity.Id) {
		msg := Msg{
			Payload: string(data),
			Src:     node.identity.Address(),
			Dst:     node.successor.Address(),
			Method:  MTH_SETPRED}
		node.transport.send(&msg)
	}
}

// Stabilizes the node, frequently checks that the successor == successor.predecessor
func (node *DHTNode) stabilize() {
	if node.successor.Ip == "" {
		log.Printf("Node %x has no successor.", node.identity.Id)
		return
	}

	log.Printf("Node %x sends stabilize to node %x.", node.identity.Id, node.successor.Id)
	msg := Msg{
		Src:     node.identity.Address(),
		Dst:     node.successor.Address(),
		Method:  MTH_STABILIZE,
		Traffic: TRF_RSVP,
	}
	node.transport.send(&msg)

}

func (node *DHTNode) handleStabilize(in Msg) {
	log.Printf("Node %x attempts to stabilize...", node.identity.Id)
	if in.Traffic&TRF_RSVP > 0 {
		out := Msg{}
		out.Src = in.Dst
		out.Dst = in.Src
		out.Method = MTH_STABILIZE
		out.Traffic = TRF_ACK
		data, ok := node.predecessor.ToJSON()
		if ok {
			out.Payload = string(data)
		} else {
			log.Printf("Node %x failed to parse data for stabilization.", node.identity.Id)
		}
		node.transport.send(&out)
	} else if in.Traffic&TRF_ACK > 0 {
		contact, ok := in.GetContact()
		if ok {
			if node.responsible(contact.Id) && contact.Id != "" && (node.identity.Id != contact.Id) {
				log.Printf("Node %x stabilized successfully. Setting successor %x to predecessor %x", node.identity.Id, node.successor.Id, contact.Id)
				node.successor = contact
			}
		}
		node.notify()
	}
}

func (node *DHTNode) addToRing(ip, port string) {
	if ip == "" && port == "" {
		log.Printf("Node %x got no connection info. Assuming root node responsibility.", node.identity.Id)
		//node.predecessor = node.identity
		node.successor = node.identity
		node.addFingers()
		go node.doTicking()
	} else {
		//There is actually a node to contact. Find your place in the ring and enter.
		msg := Msg{
			Payload: node.identity.Id,
			Src:     node.identity.Address(),
			Dst:     ip + ":" + port,
			Method:  MTH_LOOKUP | MTH_ADD,
			Traffic: TRF_RSVP,
		}
		log.Printf("Node %x is attempting to ask %s for its place in the ring, with a return address of %s.", node.identity.Id, msg.Dst, msg.Src)
		node.transport.send(&msg)
	}
}

// Handles lookups and, depending on which flags are set, handles extra functions
func (node *DHTNode) handleLookup(message Msg) {
	if (message.Method & MTH_ADD) > 0 { // Handles addToRing-messages
		contact, _ := message.GetContact()
		node.successor = contact
		log.Printf("Node %x got a LOOKUP ACK and added itself before node %x", node.identity.Id, contact.Id)
		node.notify()

		//Start the finger generation procedure.
		node.addFingers()

		//Populate the successorlist.
		succListMsg := Msg{
			Src:     node.identity.Address(),
			Dst:     node.successor.Address(),
			Key:     0,
			Method:  MTH_SUCCRING,
			Traffic: TRF_PROP,
		}
		node.transport.send(&succListMsg)

		//Start the ticker that periodically stabilizes and updates fingers.
		go node.doTicking()
	}
	if (message.Method & MTH_UPDATE) > 0 {
		data, ok := message.GetContact()
		if !ok {
			fmt.Println("is not ok")
			return
		}
		node.fingers[message.Key] = data
	}
}

// Does a node-lookup
func (node *DHTNode) lookup(msg Msg) (out Contact) {
	if node.responsible(msg.Payload) {
		// I am responsible, return message.
		var data []byte
		if (msg.Method & MTH_ADD) > 0 {
			//log.Printf("Node %x got a LOOKUP ADD request from %s and is returning its successor (%x).", node.identity.Id, msg.Src, node.successor.Id)
			data, _ = node.successor.ToJSON()
		} else {
			//log.Printf("Node %x got a LOOKUP request from %s and is responsible for the data requested.", node.identity.Id, msg.Src)
			data, _ = node.identity.ToJSON()
		}
		msg.Payload = string(data)
		msg.Dst = msg.Src
		msg.Src = node.identity.Address()
		msg.Traffic = msg.Traffic ^ (TRF_RSVP | TRF_ACK)
		node.transport.send(&msg)
		return node.identity
	}
	// I am not responsible.
	if between([]byte(node.predecessor.Id), []byte(node.identity.Id), []byte(msg.Payload)) {
		// My predecessor is responsible
		out = node.predecessor
		//log.Printf("Node %x got a LOOKUP request from %s and defers to its predecessor.", node.identity.Id, msg.Src)

	} else if len(node.fingers) > 0 {
		var found bool
		for i, j := 0, 1; j < len(node.fingers); i, j = i+1, j+1 {
			// One of my fingers are responsible
			if node.fingers[i].Ip != "" && between([]byte(node.fingers[i].Id), []byte(node.fingers[j].Id), []byte(msg.Payload)) {
				found = true
				out = node.fingers[i]
				if out.Ip != "" {
					//log.Printf("Node %x got a LOOKUP request from %s and defers to a finger.", node.identity.Id, msg.Src)
					break
				} else {
					//log.Printf("Node %x got a LOOKUP request from %s that defers to a dummy finger, overriding to use successor.")
					out = node.successor
					break
				}
			}
		}
		if !found && node.fingers[len(node.fingers)-1].Ip != "" {
			// none of my fingers are responsible, defer to last finger
			out = node.fingers[len(node.fingers)-1]
			log.Printf("Node %x got a LOOKUP request from %s, can't see the data, and defers to its last finger.", node.identity.Id, msg.Src)
		}
	} else {
		// I have no fingers, defer to successor
		out = node.successor
		log.Printf("Node %x got a LOOKUP request from %s, but has no fingers. It defers to its successor.", node.identity.Id, msg.Src)
	}

	// send message
	msg.Dst = out.Address()
	node.transport.send(&msg)
	return
}

func (node *DHTNode) responsible(Payload string) bool {
	a := between([]byte(node.identity.Id), []byte(node.successor.Id), []byte(Payload))
	return a
}

func (node *DHTNode) printRing() {
	log.Printf("Node %x is propagating a PRINT to %s.", node.identity.Id, node.successor.Address())
	msg := Msg{
		Payload: node.identity.Id,
		Src:     node.identity.Address(),
		Dst:     node.successor.Address(),
		Method:  MTH_PRINT,
	}
	node.transport.send(&msg)
}

func (node *DHTNode) handlePrint(message Msg) {
	if node.successor.Address() != message.Src {
		if node.successor.Ip == "" {
			log.Printf("Node %x has no successor. This is probably a bad thing.", node.identity.Id)
		}
		message.Payload += (", " + node.identity.Id)
		message.Dst = node.successor.Address()
		node.transport.send(&message)
	} else {
		fmt.Println("PRINT:", message.Payload+", "+node.identity.Id)
	}
}

func (node *DHTNode) addFingers() {
	log.Printf("Node %x is calculating %d fingers.", node.identity.Id, bits)
	for i := 0; i < bits; i++ {
		//Calculate the key.
		_, key := calcFinger([]byte(node.identity.Id), i, bits)
		fmt.Printf("Node %v Adding dummy finger number %v: key = %x\n", node.identity.Id, i, key)
		//Add dummy key
		dummy := Contact{
			Id:   string(key),
			Ip:   "",
			Port: "",
		}
		node.fingers[i] = dummy

		//Send a message asking for who is responsible for this key.
		msg := Msg{
			Payload: string(key),
			Src:     node.identity.Address(),
			Method:  MTH_UPDATE | MTH_LOOKUP,
			Traffic: TRF_RSVP,
			Key:     i,
		}
		node.lookup(msg)
	}
}

func (node *DHTNode) testCalcFingers(m int, bits int) {
	finger, _ := calcFinger([]byte(node.identity.Id), m, bits)
	msg := Msg{Payload: finger}
	next := node.lookup(msg)
	fmt.Println("successor    " + next.Id)
	dist := distance([]byte(node.identity.Id), []byte(next.Id), bits)
	fmt.Println("distance     " + dist.String())
}

//fixFingers calculates a new finger for a random index in the nodes list of fingers.
func (node *DHTNode) fixFingers() {
	r := rand.Intn(bits)
	_, key := calcFinger([]byte(node.identity.Id), r, bits)
	msg := Msg{
		Payload: string(key),
		Src:     node.identity.Address(),
		Method:  MTH_LOOKUP | MTH_UPDATE,
		Traffic: TRF_RSVP,
		Key:     r,
	}
	node.lookup(msg)
}

//Ticker runs stabilize and fixFingers at set intervals.
func (node *DHTNode) doTicking() {
	log.Printf("Node %x is now ticking.", node.identity.Id)
	for range node.ticker.C {
		node.stabilize()
		node.fixFingers()
	}
}

func (node *DHTNode) succListAndPropagate(message Msg) {
	//Send a message with your contact info to the original asking node and x telling it which position you are meant for.
	x := message.Key
	retval, _ := node.identity.ToJSON()
	ansMsg := Msg{
		Payload: string(retval),
		Src:     message.Dst,
		Dst:     message.Src,
		Key:     x,
		Method:  MTH_SUCCRING,
	}
	node.transport.send(&ansMsg)
	log.Printf("Node %x got a successor list message with counter %v. Answering and forwarding if neccesary.\n", node.identity.Id, x)

	//	Forward the succlist message to your successor if appropriate (if x is still smaller than the desired amount of successors).
	x += 1
	if x < successors {
		log.Printf("Node %x forwarding the successor list message to successor.\n", node.identity.Id)
		propMsg := Msg{
			Src:     message.Src,
			Dst:     node.successor.Address(),
			Key:     x,
			Method:  MTH_SUCCRING,
			Traffic: TRF_PROP,
		}
		node.transport.send(&propMsg)
	}
}

//Takes an id and puts it on the correct place in the successor list.
func (node *DHTNode) gotNewSucclistMember(message Msg) {
	con, ok := message.GetContact()
	position := message.Key
	if ok {
		log.Printf("Node %x got a new successor list member with id %x, placing the member in position %v.\n", node.identity.Id, con.Id, position)
		node.succList[position] = con
	} else {
		log.Printf("Node %x got an invalid successor lit member aimed at postion %v", node.identity.Id, position)
	}
}

func (d *DHTNode) DebugString() string {
	return fmt.Sprintf("I am node %x (\"%[1]s\"). I am connected to \"%s\" and \"%s\".", d.identity.Id, d.predecessor.Id, d.successor.Id)
}
