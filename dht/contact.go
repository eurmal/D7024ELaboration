package dht

import (
	"encoding/json"
	"log"
)

type Contact struct {
	Id   string `json:"id"`
	Ip   string `json:"ip"`
	Port string `json:"port"`
}

func (c *Contact) ToJSON() (out []byte, ok bool) {
	out, err := json.Marshal(c)
	if err != nil {
		log.Printf("Failed to marshal contact  %v!", c.Id)
		return []byte{}, false
	}
	return out, true
}

func (c *Contact) Address() string {
	return c.Ip + ":" + c.Port
}
