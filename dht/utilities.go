package dht

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"log"
	"math/big"
)

func distance(a, b []byte, bits int) *big.Int {
	var ring big.Int
	ring.Exp(big.NewInt(2), big.NewInt(int64(bits)), nil)

	var a_int, b_int big.Int
	(&a_int).SetBytes(a)
	(&b_int).SetBytes(b)

	var dist big.Int
	(&dist).Sub(&b_int, &a_int)

	(&dist).Mod(&dist, &ring)
	return &dist
}

func between(id1, id2, Payload []byte) bool {
	// 0 if a==b, -1 if a < b, and +1 if a > b
	if bytes.Compare(Payload, id1) == 0 { // Payload == id1
		//fmt.Println("Payload is equal to id1")
		return true
	}

	if bytes.Compare(id2, id1) == 1 { // id2 > id1
		//fmt.Println("Payload smaller than id2 and larger than id1")
		return bytes.Compare(Payload, id2) == -1 && bytes.Compare(Payload, id1) == 1 // Payload < id2 && Payload > id1
	} else { // id1 > id2
		//fmt.Println("Payload larger than id1 or smaller than id2.")
		return bytes.Compare(Payload, id1) == 1 || bytes.Compare(Payload, id2) == -1 // Payload > id1 || Payload < id2
	}
}

// (n + 2^(k-1)) % (2^m)
func calcFinger(n []byte, k, m int) (string, []byte) {
	nBigInt := big.Int{}
	nBigInt.SetBytes(n)

	// x: 2^(k-1)
	two := big.NewInt(2)
	addend := big.Int{}
	addend.Exp(two, big.NewInt(int64(k-1)), nil)

	// y: n + x
	sum := big.Int{}
	sum.Add(&nBigInt, &addend)

	// z: 2^m
	ceil := big.Int{}
	ceil.Exp(two, big.NewInt(int64(m)), nil)

	// y % z
	result := big.Int{}
	result.Mod(&sum, &ceil)
	resultHex := fmt.Sprintf("%x", result.Bytes())

	//log.Printf("Finger %d:\n %s = (%s+2^(%[1]d-1)) %% (2^%d)\n", k, resultHex, nBigInt.String(), m)

	return resultHex, result.Bytes()
}

func generateNodeId() string {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	// calculate sha-1 hash
	hasher := sha1.New()
	hasher.Write([]byte(u.String()))

	return fmt.Sprintf("%x", hasher.Sum(nil))
}

func messageHandler(queue <-chan Msg, node *DHTNode) {
	log.Printf("Node %x is starting its message handler.", node.identity.Id)
	for message := range queue {
		//log.Printf("Node %s received message (%b) from %s!", node.identity.Id, message.Method, message.Src)

		/*if (message.Traffic & TRF_RSVP) > 0 {
			msg := Msg{
				Payload: "200, OK",
				Src:     node.contact.ip + ":" + node.contact.port,
				Dst:     message.Src,
				Traffic:   TRF_ACK}
			node.transport.send(&msg)
		}*/
		if (message.Method & MTH_ADD) > 0 {
		}
		if (message.Method & MTH_UPDATE) > 0 {

		}
		if (message.Method & MTH_LOOKUP) > 0 {
			if (message.Traffic & TRF_RSVP) > 0 {
				node.lookup(message)
			} else if (message.Traffic & TRF_ACK) > 0 {
				node.handleLookup(message)
			}
		}
		if (message.Method & MTH_SETPRED) > 0 {
			contact, ok := message.GetContact()
			if ok {
				log.Printf("Node %x got NOTIFY and is setting node %x as predecessor.", node.identity.Id, contact.Id)
				node.predecessor = contact
			}
		}
		if (message.Method & MTH_STABILIZE) > 0 {
			node.handleStabilize(message)
		}
		if (message.Method & MTH_PRINT) > 0 {
			node.handlePrint(message)
		}
		if (message.Method & MTH_SUCCRING) > 0 {
			if (message.Traffic & TRF_PROP) > 0 {
				node.succListAndPropagate(message)
			} else {
				node.gotNewSucclistMember(message)
			}
			node.succListAndPropagate(message)
		}
	}
}
